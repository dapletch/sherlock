package org.meddlingpi

import grails.testing.web.controllers.ControllerUnitTest
import groovy.util.logging.Slf4j
import spock.lang.Specification

@Slf4j
class ReportControllerSpec extends Specification implements ControllerUnitTest<ReportController> {

    def url

    def setup() {
        url = "http://localhost:8080"
    }

    def cleanup() {
    }

    // app must be running for unit test to pass
    void "test caseReportPdf"() {
        // reference this for the unit test
        // https://stackoverflow.com/questions/31341250/open-or-download-url-link-in-grails-controller
        setup:
        def timestamp = new Date().format('yyyy-MM-dd-hh-mm-ss')
        def file = File.createTempFile("case${timestamp}", ".pdf")

        when:
        def fos = new FileOutputStream(file)
        fos.write(new URL("${url}/report/caseReportPdf").bytes)
        log.info("fos: " + fos)

        fos.close()
        file.deleteOnExit()

        then:
        noExceptionThrown()
    }
}
