<table>
%{-- TODO make variables configurable... --}%
    <g:set var="singleValueFields" value="['date', 'hidden', 'number', 'text', 'textarea']"/>
    <g:set var="multiValueFields" value="['checkbox-group', 'radio-group', 'select']"/>
    <g:if test="${category && setting?.size() > 0}">
        <h2>${category}</h2>
    </g:if>
    <g:each var="formElement" in="${setting as List<Map>}" status="i">
        <tr>
            <td>
                <b>
                    <g:if test="${formElement?.label}">
                        ${formElement.label?.replace('&amp;', '&')}:
                    </g:if>
                    <g:elseif test="${formElement?.name}">
                        ${formElement.name}:
                    </g:elseif>
                </b>
            </td>
            <g:if test="${singleValueFields.contains(formElement?.type)}">
                <td>
                    <g:if test="${formElement?.value}">
                        ${formElement.value}
                    </g:if>
                </td>
            </g:if>
            <g:if test="${multiValueFields.contains(formElement?.type)}">
                <td>
                    <g:set var="values" value="${formElement?.values?.findAll { it?.selected == true}}"/>
                    <g:if test="${values?.size() == 1}">
                        ${values[0]?.value}
                    </g:if>
                    <g:else>
                        <ul>
                            <g:each var="value" in="${values as List<Map>}" status="j">
                                <li>${value?.value}</li>
                            </g:each>
                        </ul>
                    </g:else>
                </td>
            </g:if>
        </tr>
    </g:each>
</table>