package org.meddlingpi

import grails.converters.JSON
import org.grails.web.json.JSONObject
import org.springframework.http.HttpStatus

class ReportController {

    def caseReportPdf() {
        def params = request.JSON ?: params
        // retrieve the caseRecord from the request
        def caseRecord = params?.caseRecord as JSONObject
        log.debug("caseRecord: ${caseRecord}")
        def recCreateTimestamp = caseRecord?.recCreateTimestamp?.length() > 0 ?
                Date.parse('yyyy-MM-dd HH:mm:ss.SSS', "${caseRecord.recCreateTimestamp}".replace('T', ' ').replace('Z', '')) : null
        def timestamp = new Date().format('yyyy-MM-dd-hh-mm-ss')
        def filename = "case_${caseRecord?.userId}_${caseRecord?.caseId}_${timestamp}.pdf"
        try {
            renderPdf(
                    filename: filename,
                    template: "/report/caseReportPdf",
                    model: [
                            caseRecord: caseRecord,
                            recCreateTimestamp: recCreateTimestamp
                    ]
            )
        } catch (e) {
            e.printStackTrace()
            log.error("timestamp: $timestamp")
            log.error("filename: $filename")
            log.error("params: ${params.toString()}")
            response.setContentType("application/json")
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            Map error = [failure: true, message: e.getMessage()]
            render new JSON(error)
        }
    }
}
